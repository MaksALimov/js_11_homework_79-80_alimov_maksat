const express = require('express');
const mysqlDb = require('../mysqlDb');
const multer = require('multer');
const config = require('../config');
const {nanoid} = require('nanoid');
const path = require('path');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },

    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    },
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    const [subject_accounting] = await mysqlDb.getConnection().query('SELECT id, category_id, location_id, title FROM ??', ['subject_accounting']);
    res.send(subject_accounting);
});

router.get('/:id', async (req, res) => {
    const [subject_accounting] = await mysqlDb.getConnection().query(
        `SELECT * FROM ?? where id = ?`,
        ['subject_accounting', req.params.id]
    );
    res.send(subject_accounting[0]);
});

router.post('/', upload.single('image'), async (req, res) => {
    if (!req.body.title || !req.body.category_id || !req.body.location_id) {
        return res.status(400).send({error: 'Data must be filled'});
    }

    const subject_accountingData = {
        title: req.body.title,
        description: req.body.description,
        category_id: req.body.category_id,
        location_id: req.body.location_id,
    };

    if (req.file) {
        subject_accountingData.image = req.file.filename;
    }


    const [newResource] = await mysqlDb.getConnection().query(
        'INSERT INTO ?? (category_id, location_id, title, description, image) values (?, ?, ?, ?, ?)',
        [
            'subject_accounting',
            subject_accountingData.category_id,
            subject_accountingData.location_id,
            subject_accountingData.title,
            subject_accountingData.description, null
        ]
    );

    res.send({
        ...subject_accountingData,
        id: newResource.insertId
    });
});

router.delete('/:id', async (req, res) => {
    const [deletedResource] = await mysqlDb.getConnection().query(
        `SELECT * FROM ?? where id= ?`,
        ['subject_accounting', req.params.id]
    );

    try {
        await mysqlDb.getConnection().query(
            `DELETE FROM ?? where id = ?`,
            ['subject_accounting', req.params.id]
        );
    } catch (e) {
        return res.status(401).send({error: 'Cannot be deleted'});
    }

    res.send(['You deleted', deletedResource[0]]);
});

router.put('/:id', upload.single('image'), async (req, res) => {
    const categoriesData = {
        category_id: req.body.category_id,
        location_id: req.body.location_id,
        title: req.body.title,
        description: req.body.description,
        id: req.params.id,
    };

    if (req.file) {
        categoriesData.image = req.file.filename;
    }

    await mysqlDb.getConnection().query(
        `UPDATE ?? SET ? where id = ?`,
        ['subject_accounting', {...categoriesData}, req.params.id]
    );

    res.send(categoriesData);
});


module.exports = router;
