create database if not exists stocktaking;

use stocktaking;

create table if not exists categories (
    id int not null auto_increment primary key,
    title varchar(255) not null,
    description text null
);

create table if not exists locations (
    id int not null auto_increment primary key,
    title varchar(255) not null,
    description text null
);

create table if not exists subject_accounting (
    id int not null auto_increment primary key,
    category_id int null,
    location_id int null,
    title varchar(255) not null,
    description text null,
    image varchar(255) null,

    constraint subject_accounting_category_id_fk
    foreign key (category_id)
    references categories(id)
    on update cascade
    on delete restrict,

    constraint subject_accounting_location_id_fk
    foreign key (location_id)
    references locations(id)
    on update cascade
    on delete restrict
);
