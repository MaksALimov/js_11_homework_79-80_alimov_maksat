const express = require('express');
const mysqlDb = require('./mysqlDb');
const categories = require('./app/categories');
const locations = require('./app/locations');
const cors = require('cors');
const subject_accounting = require('./app/subject_accounting');

const port = 8000;
const app = express();

app.use(express.json());
app.use(cors());
app.use('/categories', categories);
app.use('/locations', locations);
app.use('/subject_accounting', subject_accounting);
app.use(express.static('public'));

mysqlDb.connect().catch(e => console.log(e));
app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});

