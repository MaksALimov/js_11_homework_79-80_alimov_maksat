const express = require('express');
const mysqlDb = require('../mysqlDb');
const router = express.Router();

router.get('/', async (req, res) => {
    const [categories] = await mysqlDb.getConnection().query('SELECT id, title FROM ??', ['categories']);
    res.send(categories);
});

router.get('/:id', async (req, res) => {
    const [categories] = await mysqlDb.getConnection().query
    (
        `SELECT * FROM ?? where id = ?`,
        ['categories', req.params.id]
    );
    res.send(categories[0]);
});

router.post('/', async (req, res) => {
    const categoriesData = {
        title: req.body.title,
        description: req.body.description,
    };

    if (!req.body.title) {
        return res.status(400).send({error: 'Title must be filled!'});
    }

    const [newResource] = await mysqlDb.getConnection().query(
        'INSERT INTO ?? (title, description) values (?, ?)',
        ['categories', categoriesData.title, categoriesData.description]
    );

    res.send({
        ...categoriesData,
        id: newResource.insertId
    });
});

router.delete('/:id', async (req, res) => {
    const [deletedResource] = await mysqlDb.getConnection().query(
        `SELECT * FROM ?? where id= ?`,
        ['categories', req.params.id]
    );

    try {
        await mysqlDb.getConnection().query(
            `DELETE FROM ?? where id = ?`,
            ['categories', req.params.id]
        );
    } catch (e) {
        return res.status(401).send({error: 'Cannot be deleted'});
    }

    res.send(['You deleted', deletedResource[0]]);
});

router.put('/:id', async (req, res) => {
    const categoriesData = {
        title: req.body.title,
        description: req.body.description,
        id: req.params.id,
    };

    await mysqlDb.getConnection().query(
        `UPDATE ?? SET ? where id = ?`,
        ['categories', {...categoriesData}, req.params.id]
    );

    res.send(categoriesData);
});

module.exports = router;
