const express = require('express');
const mysqlDb = require('../mysqlDb');
const router = express.Router();

router.get('/', async (req, res) => {
    const [locations] = await mysqlDb.getConnection().query('SELECT id, title FROM ??', ['locations']);
    res.send(locations);
});

router.get('/:id', async (req, res) => {
    const [location] = await mysqlDb.getConnection().query(
        `SELECT * FROM ?? where id = ?`,
        ['locations', req.params.id]
    );
    res.send(location[0]);
});

router.post('/', async (req, res) => {
    if (!req.body.title) {
        return res.status(400).send({error: 'Title must be filled'});
    }

    const locationsData = {
        title: req.body.title,
        description: req.body.description,
    };

    const [newResource] = await mysqlDb.getConnection().query(
        'INSERT INTO ?? (title, description) values (?, ?)',
        ['locations', locationsData.title, locationsData.description]
    );

    res.send({
        ...locationsData,
        id: newResource.insertId
    });
});

router.delete('/:id', async (req, res) => {
    const [deletedResource] = await mysqlDb.getConnection().query(
        `SELECT * FROM ?? where id= ?`,
        ['locations', req.params.id]
    );

    try {
        await mysqlDb.getConnection().query(
            `DELETE FROM ?? where id = ?`,
            ['locations', req.params.id]
        );
    } catch (e) {
        return res.status(401).send({error: 'Cannot be deleted'});
    }

    res.send(['You deleted', deletedResource[0]]);
});

router.put('/:id', async (req, res) => {
    const categoriesData = {
        title: req.body.title,
        description: req.body.description,
        id: req.params.id,
    };

    await mysqlDb.getConnection().query(
        `UPDATE ?? SET ? where id = ?`,
        ['locations', {...categoriesData}, req.params.id]
    );

    res.send(categoriesData);
});

module.exports = router;
